<?php

namespace App\Service\Cart;

use App\Repository\ProduitRepository;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CartService
{
    protected $session;
    protected $produitRepository;

    public function __construct(
        SessionInterface $session,
        ProduitRepository $produitRepository
    ) {
        // on intialise le constructeur 
        $this->session = $session;
        $this->produitRepository = $produitRepository;
    }

    public function add(int $id)
    {
         //je déclare une variable panier dans la session et je lui donne un tableau vide comme 
        //un deuxième paramètre
        
        $panier = $this->session->get('panier', []);   
   
        if (!empty($panier[$id])) {
            //ensuite je vérifie si le panier n'est pas vide, je peux ajouter un produit
             
            $panier[$id]++;
        } else {
            //si non j'ajoute un nouveau produit

            $panier[$id] = 1;
        }
        //ensuite j'enregistre mon panier dans la session, laquel je l'ai remplacé par une
        //nouvelle variabe qui contient les produits que je viens d'ajouter 
     
        $this->session->set('panier', $panier);
    }

    public function remove(int $id)
    {

         //j'extraie mon panier
        $panier = $this->session->get('panier', []);

        //Si le panier n’est pas vide.  
        if (!empty($panier[$id])) {
            
          //je veux supprimer mes produits avec la méthode unset()  
            unset($panier[$id]);
        }
  
       // ensuite je remplace ma session avec la nouvelle session.
        $this->session->set('panier', $panier);
    }


    public function getFullCart(): array
    {
        // j'ai un panier et se egele a ce que j'ai dans la session qui 
        // s'appele le panier que on met par defaut un [] vide
        //Here I reset my (panier) by default an empty table
        $panier = $this->session->get('panier', []);

        //here I declare a new variable where i will store my new data,
        //I gave him an empty table to show it to the user

        $panierWithData = [];

        //foreach (panier) as its id and quantity.

        foreach ($panier as $id => $quantity) {
            //Here I want to get all the products by its (id)

            $produit = $this->produitRepository->find($id);
            //if the product already exists

            if ($produit) {
                //I can add it to the new variable based on its (id) and the quantity in an associative array


                $panierWithData[] = [
                    //And this associative array contains produitRepository

                    'produit'  => $this->produitRepository->find($id),
                    'quantity' => $quantity
                ];
            } else {
                //nothing happened
            }
        }
        return $panierWithData;
    }

    // cette fonction calcule le total d'un produit ajouter à chaque fois dans le panier. 
    public function getTotal(): float
    {
        //Je déclare une variable qui s'appelle totale et qui est eguale à 0.
        $total = 0;
        //pour chaque item ajouté dans le panier va prendre son prix et faire une addition 
        //avec le prix de l'autre produit
    
        foreach ($this->getFullCart() as $item) {
            $total += $item['produit']->getPrix() * $item['quantity'];
        }

        //enfin retourner une resultat totale.
        return $total;
    }
}

