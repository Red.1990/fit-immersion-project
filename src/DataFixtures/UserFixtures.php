<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
    
        $user = new User();
        $user->setNom('Souad')
             ->setPrenom("Taki")
             ->setEmail('taki.souad-90@hotmail.com')
             ->setPassword('123')
             ->setStatut('particulier')
             ->setAdressPostale('Avenu les boulig')
             ->setCodePostal('1330')
             ->setVille('Montpellier')
             ->setPays('France')
             ->setRoles(['ROLE_ADMIN']);


         $manager->persist($user);

        $manager->flush();
    }
}
