<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class, [
                'attr' =>['placeholder' => 'Inserez votre nom ici']
            ])
            ->add('prenom', TextType::class, [
                'attr' =>['placeholder' => 'Inserez votre prenom ici']
            ])
         
         
            ->add('email', EmailType::class, [
                'attr' =>['placeholder' => 'Inserez votre adresse email ici']
            ])
            ->add('plainPassword', RepeatedType::class, [
                'required' => false,
                // instead of being set onto the object directly,
                // this is read and encoded in the controller
                'type' => PasswordType::class,
                'attr' =>['placeholder' => 'Inserez votre mot de passe  ici'],
                'invalid_message' => 'Les mots de passe ne correspondent pas',
                'mapped' => false,
                'first_options' => ['label' => 'Mot de passe'],
                'second_options' => ['label' => 'Confirmer Votre Mot de passe'],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Inserer un mot de passe',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Votre mot de passe doit contenir au moins {{ limit }} caractères',
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                    ]),
                ],
            ])
         
           
            ->add('statut', ChoiceType::class, [
        
                'choices' => [
                    'Particulier' => 'Particulier',
                    'Entrepreneur' => 'Entrepreneur',
                   
                ],
                
                'expanded' =>true,
              
            ])
            ->add('tel', TelType::class, [
                'attr' =>['placeholder' => 'Inserez votre Numero de tel']
            ])
            

            ->add('adressePostale', TextType::class, [
                'attr' =>['placeholder' => 'Inserez votre adresse postale ici']
            ])
            ->add('codePostal', TextType::class, [
                'attr' =>['placeholder' => 'Inserez votre code postal ici']
            ])
            ->add('ville', TextType::class, [
                'attr' =>['placeholder' => 'Inserez votre ville ici']
            ])
            ->add('pays', TextType::class, [
                'attr' =>['placeholder' => 'Inserez votre pays ici']
            ])
            ->add('agreeTerms', CheckboxType::class, [
                'mapped' => false,
                'constraints' => [
                    new IsTrue([
                        'message' => 'You should agree to our terms.',
                    ]),
                ],
            ])
        ;

            
        
    }
    

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
