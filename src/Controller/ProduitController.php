<?php

namespace App\Controller;

use App\Entity\Produit;
use App\Repository\ProduitRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class ProduitController extends AbstractController
{
    /**
     * @Route("/produit", name="produit_index", methods={"GET"})
     */
    public function boutique(ProduitRepository $repository): Response
    {
        //cette fonction permet de chercher les produits dans la base de données,
        //la méthode findAll() permet de chercher tous les produits 
        $produits = $repository->findAll();
        //ensuite rediréger vers la page boutique()
        return $this->render('produit/boutique.html.twig', [
         //cette variable permet de remplacer dans twig pour les afficher.
            'produits' => $produits,
        ]);
    } 

    /**
     * @Route("/produit/{id}", name="produit_detail", methods={"GET"})
     */

     public function detail(Produit $produit): Response
     {
        //if the product exists render this page (detail.html.twig)
       if (is_null($produit)){
    }
         return $this->render('produit/detail.html.twig',[
             'produit' =>$produit
         ]);
     }
   
   
  
}


