<?php

namespace App\Controller;

use App\Entity\Produit;
use App\Form\ProduitType;
use App\Repository\ProduitRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin_index", methods={"GET"})
     * 
     */
    public function index(ProduitRepository $repository): Response

    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $produits = $repository->findAll();
        return $this->render('admin/index.html.twig', [
            'produits' => $produits,
        ]);
    }
     /**
      * @Route("/produit/admin/new", name="produit_create", methods={"GET","POST"})
      *@IsGranted("ROLE_ADMIN")
      */

      public function form(Request $request, ObjectManager $manager)
      {
     
      
         $this->denyAccessUnlessGranted('ROLE_ADMIN');
          $produit = new Produit();
          $form = $this->createForm(ProduitType::class, $produit);
          $form->handleRequest($request);

      if($form->isSubmitted() && $form->isValid()){

          if($produit->getId()){
              $produit->setCreatedAt(new \DateTime());
          }
          $produit->setCreatedAt(new \DateTime());
          $manager->persist($produit);
          $manager->flush();

          return $this->redirectToRoute('admin_index');                          
          
      }

    
      return $this->render('admin/create.html.twig', [
          'produit' => $produit,
          'formProduit' =>$form->createView(),
          
      ]);

  }

  /**
   * @Route("/admin/produit/{id}", name="produit_show"), methods={"GET"},
   * requirements={"id":"\d+"})
   */
  public function show(Produit $produit): Response
{
    return $this->render('admin/show.html.twig', [
        'produit' => $produit
    ]);
}

/**
 * @Route("/admin/produit/{id}/edit", name="produit_edit", methods={"GET","POST"}, 
 * requirements={"id":"\d+"})
 */

  public function edit(Request $request, Produit $produit): Response
  {
    $this->denyAccessUnlessGranted('ROLE_ADMIN');
      $form = $this->createForm(ProduitType::class, $produit);
      $form->handleRequest($request);
      if($form->isSubmitted() && $form->isValid()){
          $this->getDoctrine()->getManager()->flush();

          return $this->redirectToRoute('admin_index', [
              'id' => $produit->getId()
          ]);
      }
      return $this->render('admin/edit.html.twig', [
          'produit' => $produit,
          'formProduit' => $form->createView(),
      ]);

  }
  /**
   * @Route("/admin/produit/{id}/delete", name="produit_delete", methods={"DELETE"})
   */

   public function delete(Request $request, Produit $produit): Response
   {
 
       $this->denyAccessUnlessGranted('ROLE_ADMIN');
       if ($this->isCsrfTokenValid('delete' .$produit->getId(),
       $request->request->get('_token'))){

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($produit);
            $entityManager->flush();
       }
       return $this->redirectToRoute('admin_index');
   }

 
}
