<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class UserController extends AbstractController
{

    /**
     * @Route("/user/register", name="user_register")
     * @return Response
     */

    public function user(Request $request, ObjectManager $manager, 
    UserPasswordEncoderInterface $passwordEncoder
    )
     {
        $user = new User();
        

        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $hash = $passwordEncoder->encodePassword($user, $form->get('plainPassword')->getData());
            $user->setPassword($hash);

            $manager->persist($user);
            $manager->flush();

            $this->addFlash(

                'success',
                "Votre compte à bien été crée! Vous pouvez vous connectez"
            );
            return $this->redirectToRoute('app_login');
        }

        return $this->render('user/register.html.twig', [
            'form' => $form->createView(),

        ]);
   
    }

}
